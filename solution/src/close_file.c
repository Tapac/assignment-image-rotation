//
// Created by taras on 18.01.23.
//
#include <stdio.h>

enum close_status  {
    CLOSE_OK = 0,
    ERROR = 1,
};

enum close_status to_close(FILE* f){
    if (!fclose(f)){
        return CLOSE_OK;
    }
    return ERROR;
}

void print_input_file_closing_errors(enum close_status status){
    if (status != CLOSE_OK){
        fprintf(stderr,"%s", "Closing error: input file was closed incorrectly");
    }
}

void print_output_file_closing_errors(enum close_status status){
    if (status != CLOSE_OK){
        fprintf(stderr,"%s", "Closing error: output file was closed incorrectly");
    }
}
