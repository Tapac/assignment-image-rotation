#include "../include/image_struct.h"
#include <stdint.h>
#include <stdlib.h>

//
// Created by taras on 18.01.23.
//
size_t padding_count(uint32_t width){
    return (size_t)(4 - (width*sizeof(struct pixel))%4);
}
