//
// Created by taras on 24.01.23.
//
#include "../include/image_struct.h"
#include "../include/padding_count.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum image_status{
    READ_OK = 0,
    WRITE_OK = 1,
    INVALID_FILE_TO_READ = 2,
    INVALID_IMAGE_TO_WRITE = 3,
    READ_INVALID_BITS = 4,
    READ_INVALID_HEADER = 5,
    INVALID_FILE_TO_WRITE = 6,
    INVALID_IMAGE_TO_READ = 7,
    WRITE_INVALID_BITS = 8,
    WRITE_INVALID_HEADER = 9
};

enum image_status read_image( FILE* in, struct image* img ){
    size_t result = 0;
    uint8_t padding[4];
    if (in == NULL) {
        return INVALID_FILE_TO_READ;
    }
    if (img == NULL) {
        return INVALID_IMAGE_TO_WRITE;
    }
    if (img->height == 0 || img->width == 0){
        return READ_INVALID_HEADER;
    }
    const size_t padding_size = padding_count(img->width);
    struct pixel* data = malloc(((sizeof(struct pixel) * img->width)+padding_size)*img->height);
    img->data = data;
    for (size_t i = 0; i < img->height ; i++){
        result = result + sizeof(struct pixel)*img->width*fread(data, sizeof(struct pixel) * img->width, 1, in);
        data = data + img->width;
        result = result + padding_size*fread(&padding, padding_size, 1, in);
    }
    if (result!=(img->width*sizeof(struct pixel)+padding_size)*img->height) {
        free(img->data);
        return READ_INVALID_BITS;
    }

    return READ_OK;
}

enum image_status write_image( FILE* out, struct image* img ){
    size_t result = 0;
    uint8_t padding[4] = {0};
    if (out == NULL) {
        return INVALID_FILE_TO_WRITE;
    }
    if (img == NULL) {
        return INVALID_IMAGE_TO_READ;
    }
    if (img->height == 0 || img->width == 0){
        return WRITE_INVALID_HEADER;
    }
    struct pixel* data = img->data;
    const size_t padding_size = padding_count(img->width);
    for (size_t i = 0; i < img->height ; i++){
        result = result + sizeof(struct pixel)*img->width*fwrite(data, sizeof(struct pixel) * img->width, 1, out);
        data = data + img->width;
        result = result + padding_size*fwrite(&padding, padding_size, 1, out);
    }
    if (result!=(img->width*sizeof(struct pixel)+padding_size)*img->height) {
        free(img->data);
        return WRITE_INVALID_BITS;
    }
    return WRITE_OK;
}


