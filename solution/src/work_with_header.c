//
// Created by taras on 24.01.23.
//
#include "../include/image_struct.h"
#include "../include/padding_count.h"
#include  <stdint.h>
#include <stdio.h>
struct  __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

enum header_status{
    READ_HEADER_OK = 0,
    WRITE_HEADER_OK = 1,
    HEADER_READING_ERROR = 2,
    HEADER_WRITING_ERROR = 3
};

static struct bmp_header make_bmp_header(struct image* img){
    struct bmp_header header = {
            .bfType = 19778,
            .bfileSize = (img->width*sizeof(struct pixel)+padding_count(img->width))*img->height,
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (img->width*sizeof(struct pixel)+padding_count(img->width))*img->height+54,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

enum header_status read_header(FILE* f, struct image* img) {
    struct bmp_header header;
    if (fread(&header, sizeof( struct bmp_header ), 1, f )) {
        img->width = header.biWidth;
        img->height = header.biHeight;
        return READ_HEADER_OK;
    }
    return HEADER_READING_ERROR;
}

enum header_status write_header( FILE* f, struct image* img) {
    struct bmp_header header = make_bmp_header(img);
    if (fwrite(&header, sizeof( struct bmp_header ), 1, f )){
        return WRITE_HEADER_OK;
    }
    return HEADER_WRITING_ERROR;
}
