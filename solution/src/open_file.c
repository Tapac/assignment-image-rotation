#include <stdio.h>
#include <string.h>

enum open_status  {
    OPEN_BMP = 0,
    WRONG_FILENAME = 1,
    WRONG_EXTENSION = 2
};

static char* errors[] = {
        "Error: wrong filename",
        "Error: wrong extension"
};

enum file_extension  {
    WRONG_NAME = 0,
    BMP = 1,
    ANOTHER_EXTENSION = 2
    //If you want to open a file which has another extension, you can write your extension here
    //and then make a function for this extension
};

static enum file_extension file_extension(const char* filename){
    const char* separator = strrchr(filename, '.');
    if (!separator || separator==filename) return WRONG_NAME;
    if (!strcmp("bmp",separator+1)){
        return BMP;
    }
    return ANOTHER_EXTENSION;
}

enum open_status open_file_to_read( const char* filename, FILE** f ) {
    if (!filename) return WRONG_FILENAME;
    if (file_extension(filename) == WRONG_NAME){
        return WRONG_FILENAME;
    } else {
        if (file_extension(filename) == ANOTHER_EXTENSION){
            return WRONG_EXTENSION;
        }
    }
    *f = fopen(filename, "rb");
    if (!*f) return WRONG_FILENAME;
    return OPEN_BMP;
}

enum open_status open_file_to_write( const char* filename, FILE** f ) {
    if (!filename) return WRONG_FILENAME;
    if (file_extension(filename) == WRONG_NAME){
        return WRONG_FILENAME;
    } else {
        if (file_extension(filename) == ANOTHER_EXTENSION){
            return WRONG_EXTENSION;
        }
    }
    *f = fopen(filename, "w");
    if (!*f) return WRONG_FILENAME;
    return OPEN_BMP;
}
void print_errors(enum open_status status){
    if (status != OPEN_BMP){
        fprintf(stderr,"%s", errors[status-1]);
    }
}

