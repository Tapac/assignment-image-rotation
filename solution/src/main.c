#include "../include/free_memory.h"
#include "../include/image_struct.h"
#include "../include/rotation.h"
#include "../include/work_with_bmp.h"
#include <stdio.h>


int main( int argc, char** argv ) {
    if (argc == 3) {
        struct image image;
        print_read_result(get_image_from_bmp(argv[1], &image));
        struct image turned_image = rotate(image);
        print_write_result(write_image_to_bmp(argv[2], &turned_image));
        print_free_errors(free_pixel_data(image.data));
        print_free_errors(free_pixel_data(turned_image.data));
    } else {
        fprintf(stderr, "%s", "Print two filenames!");
    }
    return 0;
}

