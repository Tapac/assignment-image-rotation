#include "../include/image_struct.h"
#include <stdlib.h>


struct image rotate(const struct image img ){
    struct pixel* new_data = malloc( sizeof(struct pixel) * img.height*img.width );
    for (size_t i = 0; img.height * img.width > i; i++){
        new_data[i] = img.data[img.width*(img.height-1-i%img.height)+i/img.height];
    }
    const struct image turned_image = {.height = img.width, .width = img.height, .data = new_data};
    return turned_image;
}
