#include "../include/close_file.h"
#include "../include/image_struct.h"
#include "../include/open_file.h"
#include "../include/work_with_header.h"
#include "../include/work_with_image.h"
#include <stdlib.h>

enum work_status  {
    SUCCESSFUL_WORK = 0,
    UNSUCCESSFUL_WORK = 1
};

const static char* const image_work_errors[] = {
        "Error: reading interrupted: invalid file to read",
        "Error: reading interrupted: invalid image to write",
        "Error: reading interrupted: invalid bits",
        "Error: reading interrupted: invalid header",
        "Error: writing interrupted: invalid file to write",
        "Error: writing interrupted: invalid image to read",
        "Error: writing interrupted: invalid bits",
        "Error: writing interrupted: invalid header"
};

const static char* const header_work_errors[] = {
        "Error: reading interrupted: read invalid header",
        "Error: writing interrupted: wrote invalid header"
};

static void print_image_errors(enum image_status status){
    if (!(status == READ_OK || status == WRITE_OK)){
        fprintf(stderr, "%s", image_work_errors[status-2]);
    }
}

static void print_header_errors(const enum header_status status){
    if (!(status == READ_HEADER_OK || status == WRITE_HEADER_OK)){
        fprintf(stderr, "%s", header_work_errors[status-2]);
    }
}

void print_read_result(enum work_status status){
    if (status != SUCCESSFUL_WORK){
        fprintf(stdout, "%s", "successful reading");
    }
    else {
        fprintf(stderr, "%s", "unsuccessful reading");
    }
}

void print_write_result(enum work_status status){
    if (status != SUCCESSFUL_WORK){
        fprintf(stdout, "%s", "successful writing");
    }
    else {
        fprintf(stderr, "%s", "unsuccessful writing");
    }
}

enum work_status get_image_from_bmp(const char* filename, struct image* img){
    FILE* input = NULL;
    print_errors(open_file_to_read(filename, &input));
    print_header_errors(read_header(input, img));
    print_image_errors(read_image(input, img));
    print_input_file_closing_errors(to_close(input));
    return SUCCESSFUL_WORK;
}

enum work_status write_image_to_bmp(const char* filename, struct image* img){
    FILE* output = NULL;
    print_errors(open_file_to_write(filename, &output));
    print_header_errors(write_header(output, img));
    print_image_errors(write_image(output, img));
    print_output_file_closing_errors(to_close(output));
    return SUCCESSFUL_WORK;
}
