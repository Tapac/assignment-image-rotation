//
// Created by taras on 24.01.23.
//
#include <stdio.h>
#include <stdlib.h>

enum free_status  {
    FREE_OK = 0,
    ERROR = 1,
};

enum free_status free_pixel_data(void* memblock){
    if (memblock!=NULL){
        free(memblock);
        return FREE_OK;
    }
    return ERROR;
}

void print_free_errors(enum free_status status){
    if (status != FREE_OK){
        fprintf(stderr,"%s", "Freeing error: memory block doesn't exist");
    }
}
