//
// Created by taras on 18.01.23.
//

#ifndef IMAGE_TRANSFORMER_CLOSE_FILE_H
#define IMAGE_TRANSFORMER_CLOSE_FILE_H
#include <stdio.h>
enum close_status  {
    CLOSE_OK = 0,
    ERROR = 1,
};
void print_input_file_closing_errors(enum close_status status);
void print_output_file_closing_errors(enum close_status status);

const enum close_status to_close(FILE* f);
#endif //IMAGE_TRANSFORMER_CLOSE_FILE_H
