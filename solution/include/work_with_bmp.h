#ifndef IMAGE_TRANSFORMER_WORK_WITH_BMP_H
#define IMAGE_TRANSFORMER_WORK_WITH_BMP_H
#include "../include/image_struct.h"
#include <stdio.h>
enum work_status  {
    SUCCESSFUL_WORK = 0,
    UNSUCCESSFUL_WORK = 1
};

const enum work_status get_image_from_bmp(const char* filename, struct image* img);
const enum work_status write_image_to_bmp(const char* filename, struct image* img);
void print_read_result(enum work_status status);
void print_write_result(enum work_status status);
#endif //IMAGE_TRANSFORMER_WORK_WITH_BMP_H
