//
// Created by taras on 18.01.23.
//

#ifndef IMAGE_TRANSFORMER_PADDING_COUNT_H
#define IMAGE_TRANSFORMER_PADDING_COUNT_H
#include <stdint.h>
#include <stdlib.h>

size_t padding_count(uint32_t width);
#endif //IMAGE_TRANSFORMER_PADDING_COUNT_H
