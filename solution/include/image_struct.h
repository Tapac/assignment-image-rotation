#ifndef _IMAGE_STRUCT_H_
#define _IMAGE_STRUCT_H_
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };    
#endif
