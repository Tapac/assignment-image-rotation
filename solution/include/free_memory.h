//
// Created by taras on 24.01.23.
//

#ifndef IMAGE_TRANSFORMER_FREE_MEMORY_H
#define IMAGE_TRANSFORMER_FREE_MEMORY_H
#include <stdio.h>
#include <stdlib.h>
enum free_status  {
    FREE_OK = 0,
    ERROR = 1,
};

const enum free_status free_pixel_data(void* memblock);
void print_free_errors(enum free_status status);
#endif //IMAGE_TRANSFORMER_FREE_MEMORY_H
