//
// Created by taras on 24.01.23.
//

#ifndef IMAGE_TRANSFORMER_WORK_WITH_IMAGE_H
#define IMAGE_TRANSFORMER_WORK_WITH_IMAGE_H
#include "../include/image_struct.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

enum image_status{
    READ_OK = 0,
    WRITE_OK = 1,
    INVALID_FILE_TO_READ = 2,
    INVALID_IMAGE_TO_WRITE = 3,
    READ_INVALID_BITS = 4,
    READ_INVALID_HEADER = 5,
    INVALID_FILE_TO_WRITE = 6,
    INVALID_IMAGE_TO_READ = 7,
    WRITE_INVALID_BITS = 8,
    WRITE_INVALID_HEADER = 9
};

const enum image_status read_image( FILE* in, struct image* img );

const enum image_status write_image( FILE* out, struct image* img );

#endif //IMAGE_TRANSFORMER_WORK_WITH_IMAGE_H
