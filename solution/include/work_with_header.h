//
// Created by taras on 24.01.23.
//

#ifndef IMAGE_TRANSFORMER_WORK_WITH_HEADER_H
#define IMAGE_TRANSFORMER_WORK_WITH_HEADER_H
enum header_status{
    READ_HEADER_OK = 0,
    WRITE_HEADER_OK = 1,
    HEADER_READING_ERROR = 2,
    HEADER_WRITING_ERROR = 3
};

const enum header_status read_header(FILE* f, struct image* img);
const enum header_status write_header( FILE* f, struct image* img);
#endif //IMAGE_TRANSFORMER_WORK_WITH_HEADER_H
