#ifndef _ROTATION_H_
#define _ROTATION_H_
#include <stdint.h>
#include <stdio.h>

struct image rotate(struct image img);
#endif
