#ifndef IMAGE_TRANSFORMER_OPEN_FILE_H
#define IMAGE_TRANSFORMER_OPEN_FILE_H
#include <stdio.h>

enum open_status  {
    OPEN_BMP = 0,
    WRONG_FILENAME = 1,
    WRONG_EXTENSION = 2
};

const enum open_status open_file_to_read( const char* filename, FILE** f );
const enum open_status open_file_to_write( const char* filename, FILE** f );
void print_errors(enum open_status);
#endif //IMAGE_TRANSFORMER_OPEN_FILE_H
